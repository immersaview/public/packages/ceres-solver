function(get_nuget_package PKG_ID PKG_VERSION SERVER_URL)

    set(NUGET_OUTPUT_DIR "${CMAKE_SOURCE_DIR}/packages")
    set(NUGET_PACKAGE_ROOT "${NUGET_OUTPUT_DIR}/${PKG_ID}.${PKG_VERSION}")
    if (NOT EXISTS ${NUGET_PACKAGE_ROOT})
        message(STATUS "Downloading Package ${PKG_ID}.${PKG_VERSION}.")
        execute_process(
            COMMAND nuget install "${PKG_ID}" -Version "${PKG_VERSION}" -Source "${SERVER_URL}" -OutputDirectory "${NUGET_OUTPUT_DIR}" -NonInteractive -Verbosity quiet -NoCache -DirectDownload
            RESULT_VARIABLE RES)
        if (RES)
            message(STATUS "${RES}")
            message(FATAL_ERROR "Failed to Download ${PKG_ID}.${PKG_VERSION} from ${SERVER_URL}")
        endif ()
    endif ()

    if (EXISTS ${NUGET_PACKAGE_ROOT})
        include("${NUGET_PACKAGE_ROOT}/build/native/include.cmake")
    else ()
        message(FATAL_ERROR "Package ${PKG_ID}.${PKG_VERSION} does not exist in '${NUGET_OUTPUT_DIR}'.")
    endif ()

endfunction(get_nuget_package)

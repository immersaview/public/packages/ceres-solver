cmake_minimum_required(VERSION 2.8.0)

project(ceres-solver)

#  miniglog is a minimal replacement for the recommended glog. 
# (https://github.com/google/glog)
set(MINIGLOG ON CACHE BOOL "Use miniglog" FORCE)

# Disable gflags which is needed to build examples and tests.
# (https://github.com/gflags/gflags)
set(GFLAGS FALSE CACHE BOOL "Disable gflags" FORCE)
set(BUILD_EXAMPLES FALSE CACHE BOOL "Disable examples" FORCE)
set(BUILD_TESTING FALSE CACHE BOOL "Disable testing" FORCE)

# Disable SCHUR_SPECIALIZATIONS to reduce the size of the lib
set(SCHUR_SPECIALIZATIONS FALSE CACHE BOOL "Disable testing" FORCE)

set(EIGENSPARSE FALSE CACHE BOOL "Disable EigenSparse" FORCE)

# Static library
set(BUILD_SHARED_LIBS FALSE)

set(CMAKE_POSITION_INDEPENDENT_CODE ON)

include(get_nuget_package.cmake)

# Add external libraries
get_nuget_package(Imv.External.Eigen 3.3.9.20210115 https://gitlab.com/api/v4/projects/24279179/packages/nuget/index.json)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")

# set the Eigen include directory to the 'Eigen3' target include provided 
# by Imv.External.Eigen
get_target_property(EIGEN3_INCLUDE_DIR eigen INTERFACE_INCLUDE_DIRECTORIES)
set(EIGEN3_FOUND true)
set(EIGEN3_VERSION 3.3.7)

add_library(Eigen3::Eigen ALIAS eigen)

if (WIN32)
    add_compile_options(/Z7 /MP)
endif ()

add_subdirectory(source)

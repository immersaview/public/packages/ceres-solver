**Packaging repository for Ceres Solver**

## Example CMake Usage:
```cmake
target_link_libraries(target ceres)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:ceres,INTERFACE_INCLUDE_DIRECTORIES>") 
```